import logging
import os
import time
import shutil
import copy
import pandas as pd
import sys
import pickle
import warnings
import os
import glob
import numpy as np
import pandas as pd
import utils
import warnings , os
import subprocess
import shutil
from scipy.stats import spearmanr
import tqdm
import cscore

def read_input(filename):
    df = pd.read_csv(filename, index_col=0, sep='\t')
    target_ids = []
    for each_id, each_df in df.groupby('ID'):
        if len(each_df) == 1:
            target_ids.append(each_id)
    df = df.loc[target_ids]      
    return df

def read_LINCS_data():
    df = pd.read_csv('./data/LINCS_Level5_z.tsv.gz', index_col=0, sep='\t')
    return df

def read_drug_info():
    drug_info = {}
    with open('./data/DrugBank_Compound.txt') as fp:
        for line in fp:
            sptlist = line.strip().split('\t')
            drugbank_id = sptlist[0].strip()
            cid = sptlist[1].strip()
            drug_info[cid] = drugbank_id
    return drug_info

def read_shRNA_info():
    shRNA_info = {}
    with open('./data/GSE92742_Broad_LINCS_pert_info.txt') as fp:
        for line in fp:
            sptlist = line.strip().split('\t')
            if sptlist[2].strip() == 'trt_sh':
                cid = sptlist[0].strip()
                gene = sptlist[1].strip()
                shRNA_info[cid] = gene
    return shRNA_info

def calculate_compound_similarity(input_df, l1000_df, drug_info):
    l1000_column_info = {}
    for col in l1000_df.columns:
        l1000_column_info[col] = 1
    
    results = {}
    results2 = {}
    results3 = {}
    for cell_line in tqdm.tqdm(input_df.columns):
        results[cell_line] = {}
        results2[cell_line] = {}
        results3[cell_line] = {}
        input_fc_values = input_df[cell_line].values
        
#         target_cols = []
#         for cid in drug_info:
#             key = '%s_%s'%(cell_line, cid)
#             if key in l1000_column_info:
#                 target_cols.append(key)
        
        tmp_l1000_df = l1000_df#[target_cols]
        for each_col in tqdm.tqdm(tmp_l1000_df.columns):
            if '%s'%(cell_line) in each_col:
                
                cid = each_col.split(':')[1].strip()
                cid_list = cid.split('-')
                cid = '-'.join(cid_list[0:2])

                if cid in drug_info:
                    drugbank_id = drug_info[cid]

                    new_key = '%s_%s'%(cid, drugbank_id)
                    l1000_fc_values = tmp_l1000_df[each_col].values

                    r, p = spearmanr(input_fc_values, l1000_fc_values)

                    results[cell_line][new_key] = r

                    d = {'Col1': input_fc_values, 'Col2': l1000_fc_values}
                    df = pd.DataFrame(index=l1000_df.index, data=d)

                    zhang_score = cscore.calculate_zhang_score(df)
                    results2[cell_line][new_key] = zhang_score

                    zhang_score = cscore.calculate_zhang_score(df)
                    tanimoto_sim = cscore.calculate_tanimoto(df, 100)
                    results3[cell_line][new_key] = tanimoto_sim
                
    result_df = pd.DataFrame.from_dict(results)
    result_df2 = pd.DataFrame.from_dict(results2)
    result_df3 = pd.DataFrame.from_dict(results3)
    
    return result_df, result_df2, result_df3

def calculate_shRNA_similarity(input_df):
    shRNA_info = read_shRNA_info()
    
    results = {}
    results2 = {}
    for cell_line in tqdm.tqdm(input_df.columns):
        results[cell_line] = {}
        results2[cell_line] = {}
        input_fc_values = input_df[cell_line].values
        
        l1000_df = pd.read_csv('./data/shRNA_datasets/%s_z_score.tsv.gz'%(cell_line), index_col=0, sep='\t')
        l1000_df = l1000_df.loc[input_df.index]
        
        for each_col in tqdm.tqdm(l1000_df.columns):
            shrna_id = each_col.split(':')[1].strip()
            if shrna_id in shRNA_info:
                gene_symbol = shRNA_info[shrna_id]
                l1000_fc_values = l1000_df[each_col].values
                r, p = spearmanr(input_fc_values, l1000_fc_values)
                results[cell_line][gene_symbol] = r
                
                d = {'Col1': input_fc_values, 'Col2': l1000_fc_values}
                df = pd.DataFrame(data=d)

                zhang_score = cscore.calculate_zhang_score(df)
                results2[cell_line][gene_symbol] = zhang_score
    
    result_df = pd.DataFrame.from_dict(results)
    result_df2 = pd.DataFrame.from_dict(results2)
    
    return result_df, result_df2

def main():   
    warnings.filterwarnings(action='ignore')
    
    start = time.time()
    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
    
    parser = utils.argument_parser()
    
    options = parser.parse_args()
    input_file = options.input_file
    output_dir = options.output_dir
    
    try:
        os.mkdir(output_dir)
    except:
        pass
    
    drug_info = read_drug_info()
    
    input_df = read_input(input_file)
    l1000_df = read_LINCS_data()
    
    common_lm_genes = list(set(input_df.index) & set(l1000_df.index))
    logging.info('No. of lm genes : %s' % len(common_lm_genes))
    
    input_df = input_df.loc[common_lm_genes]
    l1000_df = l1000_df.loc[common_lm_genes]

    result_df, result_df2, result_df3 = calculate_compound_similarity(input_df, l1000_df, drug_info)
    result_df.to_csv(output_dir+'/result_compound_spearman.csv')
    result_df2.to_csv(output_dir+'/result_compound_zhang.csv')
    result_df3.to_csv(output_dir+'/result_compound_tanimoto.csv')
    
#     result_df, result_df2 = calculate_shRNA_similarity(input_df)
#     result_df.to_csv(output_dir+'/result_shRNA_spearman.csv')
#     result_df2.to_csv(output_dir+'/result_shRNA_zhang.csv')
    
    logging.info(time.strftime("Elapsed time %H:%M:%S", time.gmtime(time.time() - start)))

if __name__ == '__main__':
    main()