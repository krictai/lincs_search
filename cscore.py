import pandas as pd
import copy
from scipy.stats import spearmanr

def calculate_ZhangScore(a, b):
    N = len(a)
    m = len(b)

    total = 0.0
    for i in range(1, m+1):
        total += a[i-1] * b[i-1]
    max_total = 0.0
    
    # max score
    for i in range(1, m+1):
        max_total+= (N-i+1)*(m-i+1)
    return total/max_total

def get_ZhangScore(df):
    col1 = df.columns[0]
    col2 = df.columns[1]
    
    df1 = df.sort_values(by=[col1], ascending=False)
    ref_ranks = df1[col1].values
    query_ranks = df1[col2].values
    ZhangScore = calculate_ZhangScore(ref_ranks, query_ranks)
    
    return ZhangScore

def calculate_zhang_score(df):
    tmp_df = copy.deepcopy(df)
    tmp_df[tmp_df >= 0] = 1
    tmp_df[tmp_df < 0] = -1
    
    new_df1 = df.abs()
    new_df2 = new_df1.rank()
    new_df3 = new_df2.multiply(tmp_df)
    score = get_ZhangScore(new_df3)
    return score

def calculate_tanimoto(df, gene_num=100):
    col1 = df.columns[0]
    col2 = df.columns[1]
    
    df1_large = df.nlargest(gene_num, col1)
    df2_large = df.nlargest(gene_num, col2)
    
    df1_small = df.nsmallest(gene_num, col1)
    df2_small = df.nsmallest(gene_num, col2)
    sim1 = calc_tanimoto(list(df1_large.index), list(df2_large.index))
    sim2 = calc_tanimoto(list(df1_small.index), list(df2_small.index))
    return (sim1+sim2)/2.0

def calc_tanimoto(list1, list2):
    sim = len(set(list1)&set(list2))/len(set(list1)|set(list2))
    return sim