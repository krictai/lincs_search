#Transcriptome-based drug repositioning 

##Procedure

**Note**: 
This source code was developed in Linux, and has been tested in Ubuntu 18.04 with Python 3.6.

1. Clone the repository

        git clone https://bitbucket.org/krictai/lincs_search.git

2. Create and activate a conda environment

        conda env create -f environment.yml
        conda activate lincs_search

##Example

- Run program

        python main.py -i ./input/KR37524.tsv -o ./output_KR37524

